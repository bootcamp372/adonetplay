﻿
-- Centene SQL Language Workbook: p. 81 Execercixe 3

-- Query #1.

--SELECT p.ProductID, p.ProductName, p.UnitPrice, c.CategoryName FROM Products p INNER JOIN Categories c ON p.CategoryID = c.CategoryID

--ORDER BY c.CategoryName ASC, p.ProductName;

-- Query #2.

--SELECT p.ProductID, p.ProductName, p.UnitPrice, s.CompanyName FROM Products p INNER JOIN Suppliers s ON p.SupplierID = s.SupplierID

--WHERE p.UnitPrice > 75.00 ORDER BY p.ProductName;

-- Query #3.

--SELECT p.ProductID, p.ProductName, p.UnitPrice, c.CategoryName, s.CompanyName
-- FROM Products p 
--INNER JOIN Categories c ON p.CategoryID = c.CategoryID
--INNER JOIN Suppliers s ON p.SupplierID = s.supplierID
--ORDER BY p.ProductName DESC;

-- Query #4.

--SELECT p.ProductID, p.ProductName, p.UnitPrice, c.CategoryName
--FROM Products as p 
--INNER JOIN Categories as c ON p.CategoryID = c.CategoryID
--WHERE p.UnitPrice IN
--SELECT max(p2.UnitPrice) FROM Products as p2);

-- Query #5.
SELECT o.OrderID, o.ShipName, o.ShipAddress, s.CompanyName
FROM Orders as o
JOIN Shippers AS s on s.ShipperID = o.ShipVia
WHERE o.ShipCountry = 'Germany';