﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Numerics;

namespace ADONETPlay
{
    internal class Datalayer
    {
        public static string? GetConnectionString()
        {
            // Set up a configuration object that can read your
            // appsettings.json file
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false,
            reloadOnChange: true);
            IConfiguration config = builder.Build();

            string? connStr = config["ConnectionStrings:Northwind"];
            if (connStr != null)
            {
                Console.WriteLine(connStr);
            }
            return connStr;
        }

        public static void DisplayCategoryCount(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql = "Select Count(*) From Categories";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int catgCount = (int)cmd.ExecuteScalar();
            Console.WriteLine($"There are {catgCount} categories.");
            cn.Close();
        }

        public static void DisplayTotalInventoryValue(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql = "Select Sum(UnitPrice * UnitsInStock) from Products";
            SqlCommand cmd = new SqlCommand(sql, cn);
            decimal sum = (decimal)cmd.ExecuteScalar();
            Console.WriteLine($"Inventory sum is {sum:C}.");
            cn.Close();
        }

        public static void AddAShipper(string connStr)
        {
            Console.WriteLine("Enter company name:");
            string name = Console.ReadLine();
            Console.WriteLine("Enter company phone:");
            Int32 phone = Convert.ToInt32(Console.ReadLine());
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            /*            "INSERT INTO Shippers (CompanyName, Phone) VALUES('Test Company', 1231231234); ";*/
            "INSERT INTO Shippers (CompanyName, Phone) VALUES('" + name + "', " + phone + "); ";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");
            cn.Close();
        }

        public static void ChangeShipperName(string connStr)
        {
            Console.WriteLine("Enter old company name:");
            string oldName = Console.ReadLine();
            Console.WriteLine("Enter new company name:");
            string newName = Console.ReadLine();
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
                "Update Shippers Set CompanyName = '" + newName + "' " +
                "Where CompanyName = '" + oldName + "'";
            /*            "Update Shippers Set CompanyName = 'Fast Pack' " +
                        "Where CompanyName = 'Test Company'";*/
            SqlCommand cmd = new SqlCommand(sql, cn);
            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");
            cn.Close();
        }

        public static void DeleteShippper(string connStr)
        {
            Console.WriteLine("Enter name of company to delete:");
            string name = Console.ReadLine();
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            "DELETE FROM Products WHERE CompanyName='" + name + "';";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");
            cn.Close();
        }

        public static void DisplayAllProducts(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            "Select ProductID,ProductName,QuantityPerUnit,UnitPrice FROM Products";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                int productID = (int)dr["ProductID"];
                // or productID = Convert.ToInt32(dr["ProductID"])
                string productName = (string)dr["ProductName"];
                // or productName = dr["ProductName"].ToString();
                string quantityPerUnit = (string)dr["QuantityPerUnit"];
                // or quantityPerUnit = dr["QuantityPerUnit"].ToString();
                decimal unitPrice = (decimal)dr["UnitPrice"];
                // or unitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                Console.WriteLine(
                $"[{productID}] {productName} - {unitPrice:C} - {quantityPerUnit}");
            }
            dr.Close();

            cn.Close();
        }

        public static void DisplayProductsInCategory(string connStr, string catgName)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
                            "Select ProductID,ProductName,QuantityPerUnit,UnitPrice FROM Products JOIN Categories ON Categories.CategoryID=Products.CategoryID Where Categories.CategoryName =  @catgName";
            /*            "Select ProductID,ProductName,QuantityPerUnit,UnitPrice FROM Products JOIN Categories ON Categories.CategoryID=Products.CategoryID Where Categories.CategoryName = '" + catgName + "';";*/
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlParameter param =
            new SqlParameter("@catgName", SqlDbType.VarChar);

            param.Value = catgName;
            cmd.Parameters.Add(param);

            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                int productID = (int)dr["ProductID"];
                // or productID = Convert.ToInt32(dr["ProductID"])
                string productName = (string)dr["ProductName"];
                // or productName = dr["ProductName"].ToString();
                string quantityPerUnit = (string)dr["QuantityPerUnit"];
                // or quantityPerUnit = dr["QuantityPerUnit"].ToString();
                decimal unitPrice = (decimal)dr["UnitPrice"];
                // or unitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                Console.WriteLine(
                $"[{productID}] {productName} - {unitPrice:C} - {quantityPerUnit}");
            }
            dr.Close();

            cn.Close();

        }

        public static void DisplayProductsForSupplier(string connStr, string supplierNameInput)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            "Select p.ProductID, p.ProductName, p.UnitsInStock, p.UnitPrice, s.CompanyName, c.CategoryName " +
            "FROM Suppliers s " +
            "INNER JOIN Products p ON s.SupplierID = p.SupplierID " +
            "INNER JOIN Categories c ON p.CategoryID = c.CategoryID " +
            "Where s.CompanyName = @supplierName;";
            /*            "Where s.CompanyName = '" + supplierName + "';";*/
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlParameter param =
            new SqlParameter("@supplierName", SqlDbType.VarChar);

            param.Value = supplierNameInput;
            cmd.Parameters.Add(param);

            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                int productID = (int)dr["ProductID"];
                string productName = (string)dr["ProductName"];
                Int16 unitsInStock = (Int16)dr["UnitsInStock"];
                decimal unitPrice = (decimal)dr["UnitPrice"];
                string companyName = (string)dr["CompanyName"];
                string categoryName = (string)dr["CategoryName"];


                Console.WriteLine(
                $"[{productID}] {productName} - Price: {unitPrice:C} - Units in Stock: {unitsInStock}");
                Console.WriteLine(
                $"Company Name: {companyName} - Category Name: {categoryName}");
            }
            dr.Close();

            cn.Close();
        }

        public static void DisplaySupplierProductCount(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            "Select s.CompanyName, count(p.SupplierID) as numberOfItems from Products p " +
            "JOIN Suppliers s ON s.SupplierID = p.SupplierID " +
            "GROUP BY s.CompanyName" + ";";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string companyName = (string)dr["CompanyName"];
                Int32 numItems = (Int32)dr["numberOfItems"];

                Console.WriteLine(
                $"Company Name: {companyName} - Number of Items: {numItems}");
            }
            dr.Close();

            cn.Close();
        }

        public static void DisplayCustomersandSuppliersByCity(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql =
            "Select City, CompanyName, ContactName from [Customer and Suppliers by City];";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string companyName = (string)dr["CompanyName"];
                string city = (string)dr["City"];
                string contactName = (string)dr["ContactName"];

                Console.WriteLine(
                $"Company Name: {companyName} - City: {city} - Contact Name: {contactName}");
            }
            dr.Close();

            cn.Close();
        }

        public static void DisplayTenMostExpensiveProducts(string connStr)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            SqlCommand cmd =
            new SqlCommand("Ten Most Expensive Products", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string productName = (string)dr["TenMostExpensiveProducts"];
                decimal unitPrice = (decimal)dr["UnitPrice"];
                Console.WriteLine($"{productName} -- {unitPrice:C}");
            }

            dr.Close();
            cn.Close();
        }

        public static void DisplaySalesByCategory(string connStr, string catgName, string year)
        {
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            SqlCommand cmd =
            new SqlCommand("SalesByCategory", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("CategoryName", catgName);
            cmd.Parameters.AddWithValue("OrdYear", year);

            SqlDataReader dr = cmd.ExecuteReader();
            Console.WriteLine($"Sales for {catgName} in {year}");
            Console.WriteLine("-----------------");
            while (dr.Read())
            {
                string productName = (string)dr["ProductName"];
                decimal totalPurchase = (decimal)dr["TotalPurchase"];

                Console.WriteLine($"{productName} - {totalPurchase:C}");
            }

            dr.Close();
            cn.Close();
        }

    }

}
