﻿using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;

namespace ADONETPlay
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string connStr = Datalayer.GetConnectionString();
            string command = "";

            while (command != "Q") {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1 - Display Category Count");
                Console.WriteLine("2 - Display Total Inventory Value");
                Console.WriteLine("3 - Add a Shipper");
                Console.WriteLine("4 - Change Shipper Name");
                Console.WriteLine("5 - Delete Shipper");
                Console.WriteLine("6 - Display all Products");
                Console.WriteLine("7 - Display Products in Category");
                Console.WriteLine("8 - Display Products for Supplier");
                Console.WriteLine("9 - Display Supplier Product Count");
                Console.WriteLine("10 - Display Customers and Suppliers by City");
                Console.WriteLine("11 - Display 10 Most Expensive Products");
                Console.WriteLine("12 - Display Sales by Category");
                Console.WriteLine("Q - Quit");
                Console.WriteLine("Enter your command:");
                command = Console.ReadLine().ToUpper();
                string catgName = "";
                switch (command)
                {
                    case "1":
                        Datalayer.DisplayCategoryCount(connStr);
                        break;
                    case "2":
                        Datalayer.DisplayTotalInventoryValue(connStr);
                        break;
                    case "3":
                        Datalayer.AddAShipper(connStr);
                        break;
                    case "4":
                        Datalayer.ChangeShipperName(connStr);
                        break;
                    case "5":
                        Datalayer.DeleteShippper(connStr);
                        break;
                    case "6":
                        Datalayer.DisplayAllProducts(connStr);
                        break;
                    case "7":
                        Console.WriteLine("Enter a category name:");
                        catgName = Console.ReadLine();
                        Datalayer.DisplayProductsInCategory(connStr, catgName);
                        break;
                    case "8":
                        Console.WriteLine("Enter a supplier name:");
                        string supplierName = Console.ReadLine();
                        Datalayer.DisplayProductsForSupplier(connStr, supplierName);
                        break;
                    case "9":
                        Datalayer.DisplaySupplierProductCount(connStr);
                        break;
                    case "10":
                        Datalayer.DisplayCustomersandSuppliersByCity(connStr);
                        break;
                    case "11":
                        Datalayer.DisplayTenMostExpensiveProducts(connStr);
                        break;
                    case "12":
                        Console.WriteLine("Enter a category name:");
                        catgName = Console.ReadLine();
                        Console.WriteLine("Enter a year:");
                        string year = Console.ReadLine();
                        Datalayer.DisplaySalesByCategory(connStr, catgName, year);
                    case "Q":
                        return;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                }

                SqlConnection conn = new SqlConnection(connStr);
                conn.Open();
                Console.WriteLine("success");
                conn.Close();
            }
        }
    }
}